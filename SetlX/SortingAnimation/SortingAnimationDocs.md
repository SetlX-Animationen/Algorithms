# AFX Ledger

Um die Animationen in AFX zu generalisieren wird ein Buchhaltungsobjekt (engl. ledger) genutzt, welches den Ablauf des Sortieralgorithmus zu sichern.

## Typen von Tupel

Der Ledger besteht aus einer Liste von Tupeln. Jedes Tupel stellt dabei einen Schritt dar. Es gibt verschiedene Typen von Tupeln. Diese Typen werden durch das erste Element des Tupels spezifiziert. 

Bisher gibt es die folgenden drei Typen von Tupeln, welche jedoch noch "work in progress" sind:

### Move Tupel
```
["move",<Zahl>,<Index>]
```
Das Move Tupel bewegt die Zahl `<Zahl>` an Index `<Index>`. Ist der `<Index> < IndexOf(<Zahl>)` dann werden alle Zahlen deren Index größer oder gleich `<Index>` und kleiner `IndexOf(<Zahl>)` um eine Stelle nach rechts verschoben. Es gilt also:  
```
for(index in {1 .. #list} | index >= <Index> && index < IndexOf(<Zahl>)){
	index++;
}
```

### Swap Tupel
```
["swap",<Zahl1>,<Zahl2>]
```
Das Swap Tupel tauscht die Position von `<Zahl1>` und `<Zahl2>`. 


### Compare Tupel
```
["compare",<Zahl1>,<Zahl2>]
```
Das Compare Tupel wird genutzt um `<Zahl1>` und `<Zahl2>` hervor zu heben und so einen Vergleich zu demonstrieren.
	
### Pivot Tupel
```
["pivot",<PivotElement>,<ListeKleiner>,<ListeGroesser>]
```
Das Pivot Tupel wird lediglich im Quicksort benutzt. Es enthält neben dem Keyword `"pivot"`, das `<PivotElement>`, welches eine Zahl darstellt. Weiter ist noch eine Liste `<ListeKleiner>` mit allen Elementen, die kleiner als das Pivotelement sind und eine Liste `<ListeGroesser>` mit allen Elementen die größer als das Pivotelement sind.

# Farbauswahl
Die Farben der Balken sollen von Grün bis Blau reichen. Um dies möglichst einfach umzusetzen, soll der HSL Farbraum genutzt werden, da dort lediglich der Hue Wert geändert werden muss. Dabei beginnt die Farbe Grün bei ca. 110 und Blau endet bei ca. 250
```
m = (250 - 110) / (#list-1)
b = 110
x = index - 1

hue = m * x + b
```


# Quicksort

Für den Quicksort wird ein spezielles Pivot Tupel verwendet, da der Quicksort nur schwer verständlichen dazustellen ist. Diese Pivot Tupel hat folgende Form `["pivot", <PivotElement>, <SmallerListe>, <BiggerListe>]`
## Beispiel für Quicksort Animation:

### Schritt 1 (3 Pivot Element)
```
3 4 2 5 1 	Start
0 0 0 0 0

0 4 2 5 1	Pivot Element an richtige Position 
0 0 3 0 0 

0 4 0 5 0	Menge Smaller (2,1) vor Pivot Element
2 1 3 0 0

0 0 0 0 0	Menge Bigger (4,5) hinter Pivot Element
2 1 3 4 5

2 1 3 4 5	Alle Zahlen > 0 unten nach oben verschieben
0 0 0 0 0
```

### Schritt 2 (3 sortiert, 2 Pivot Element)
```
2 1 3 4 5	Start
0 0 0 0 0

0 1 3 4 5	Pivot Element an richtige Position
0 2 0 0 0

0 0 3 4 5	Menge Smaller (1) vor Pivot Element
1 2 0 0 0

0 0 3 4 5	Menge Bigger () hinter Pivot Element
1 2 0 0 0

1 2 3 4 5	Alle Zahlen > 0 unten nach oben verschieben
0 0 0 0 0
```

### Schritt 3 (3,2 sortiert, 1 Pivot Element)
```
1 2 3 4 5	Start
0 0 0 0 0

0 2 3 4 5	Pivot Element an richtige Position
1 0 0 0 0

0 2 3 4 5	Menge Smaller () vor Pivot Element
1 0 0 0 0

0 2 3 4 5	Menge Bigger () hinter Pivot Element
1 0 0 0 0

1 2 3 4 5	Alle Zahlen > 0 unten nach oben verschieben
0 0 0 0 0
```

### Schritt 3 (3,2,1 sortiert, 4 Pivot Element)
```
1 2 3 4 5	Start
0 0 0 0 0

1 2 3 0 5	Pivot Element an richtige Position
0 0 0 4 0

1 2 3 0 5	Menge Smaller () vor Pivot Element
0 0 0 4 0

1 2 3 0 0	Menge Bigger (5) hinter Pivot Element
0 0 0 4 5

1 2 3 4 5	Alle Zahlen > 0 unten nach oben verschieben
0 0 0 0 0
```

### Schritt 4 (4,3,2,1 sortiert, 5 Pivot Element)
```
1 2 3 4 5	Start
0 0 0 0 0

1 2 3 4 0	Pivot Element an richtige Position
0 0 0 0 5

1 2 3 4 0	Menge Smaller () vor Pivot Element
0 0 0 0 5

1 2 3 4 0	Menge Bigger () hinter Pivot Element
0 0 0 0 5

1 2 3 4 5	Alle Zahlen > 0 unten nach oben verschieben
0 0 0 0 0
```

