\chapter{Priority Queues \label{chap:prioqueue}}
In order to introduce \href{https://en.wikipedia.org/wiki/Priority_queue}{priority queues},
we first take a look at ordinary
\href{https://en.wikipedia.org/wiki/Queue_(abstract_data_type)}{queues}.
Basically, a queue can be viewed as a list with the following restrictions:
\begin{enumerate}
\item A new element can only be appended at the end of the list.
\item Only the element at the beginning of the list can be removed.
\end{enumerate}
This is similar to the queue at a cinema box office.  There, a queue is a long line of people
waiting to buy a ticket.  The person at the front of the queue is served and thereby removed from
the queue.  New persons entering the cinema have to line up at the end of the queue.  In contrast, a
priority queue is more like a dentist's waiting room.  If you have an appointment at 10:00 and you
have already waited for an hour, suddenly a patient with no appointment but a private insurance
shows up.  Since this patient has a higher priority, she will be attended next while you have
to wait for another hour. 

Priority queues have many applications in computer science.  We will make use of priority queues,
first, when implementing Huffman's algorithm for data compression and, second, when we implement
Dijkstra's algorithm for finding the shortest path in a weighted graph.  Furthermore, priority
queues are used in simulation systems and in operating systems when scheduling processes.
Finally, the sorting algorithm \blue{heapsort} is based on priority queues.

\section[Formal Definition]{Formal Definition of the ADT \textsl{PrioQueue}}
Next, we give a formal definition of the ADT $\mathtt{PrioQueue}$.  Since the data type
$\mathtt{PrioQueue}$ is really just an auxiliary data type, the definition we give is somewhat
restricted: We will only specify those functions that are needed for the algorithms of Dijkstra
and Huffman.

\begin{Definition}[Priority Queue] \hspace*{\fill} \\
  The abstract data type of priority queues is defined as follows:
  \begin{enumerate}
  \item The name is $\mathtt{PrioQueue}$.
  \item The set of type parameters is \\[0.1cm]
        \hspace*{1.3cm} $\{ \mathtt{Priority}, \mathtt{Value} \}$.

        Furthermore, there must exist a linear ordering $\leq$ on the set $\mathtt{Priority}$.
        This is needed since we want to compare the priority of different elements.
  \item The set of function symbols is \\[0.1cm]
       \hspace*{1.3cm} 
       $\{ \mathtt{prioQueue}, \mathtt{insert}, \mathtt{remove}, \mathtt{top}, \mathtt{isEmpty} \}$.
  \item The type specifications of these function symbols is given as follows:
        \begin{enumerate}
        \item $\mathtt{prioQueue}: \mathtt{PrioQueue}$

              This function is the constructor. It creates a new, empty priority queue.
        \item $\mathtt{top}: \mathtt{PrioQueue}  \rightarrow (\mathtt{Priority} \times \mathtt{Value}) \cup \{\Omega\}$

              The expression $Q.\mathtt{top}()$ returns a pair $\pair(p,v)$.  Here,  $v$ is any
              element of $Q$ that has a  maximal priority among all elements in $Q$, while $p$ is
              the priority associated with $v$. 
        \item $\mathtt{insert}: \mathtt{PrioQueue} \times \mathtt{Priority} \times \mathtt{Value} \rightarrow \mathtt{PrioQueue}$

              The expression $Q.\mathtt{insert}(p,v)$ inserts the  element $v$ into the priority queue $Q$.
              Furthermore, the priority of $v$ is set to be $p$.
        \item $\mathtt{remove}: \mathtt{PrioQueue} \rightarrow \mathtt{PrioQueue}$

              The expression $Q.\mathtt{remove}()$ removes from $Q$ the element that is returned by
              $Q.\texttt{top}()$.
        \item $\mathtt{isEmpty}: \mathtt{PrioQueue} \rightarrow \mathbb{B}$

              The expression $Q.\mathtt{isEmpty}$ checks whether the priority queue $Q$ is empty.
        \end{enumerate}
\item Before we are able to specify the behaviour of the functions implementing the function symbols
      given above, we have to discuss the notion of \blue{priority}.  We assume that there exists
      a set $\mathtt{Priority}$ and there is a linear order $\leq$ defined on this set.
      If  $p_1 < p_2$, then the priority $p_1$ is \blue{higher} than the priority $p_2$.  This
      nomenclature might seem counter intuitive.  It is motivated by 
      Dijkstra's algorithm which is discussed later.  In Dijkstra's algorithm, the priorities are
      distances in a graph and the priority of a node is higher if the node is nearer to the source
      node and in that case the distance to the source is smaller.

      In order to specify the behaviour of the functions $\mathtt{top}$, $\mathtt{insert}$, $\mathtt{remove}$,
      and $\mathtt{isEmpty}$ we need to introduce two auxiliary functions:  
      \begin{enumerate}
      \item The function $\mathtt{toList}$ turns a queue into a sorted list.  It has the signature
            \\[0.2cm]
            \hspace*{1.3cm}
            $\mathtt{toList}: \mathtt{PrioQueue} \rightarrow \mathtt{List}(\mathtt{Priority} \times \mathtt{Value})$.
            \\[0.2cm]
            This function takes a priority queue and turns this priority queue into a list of pairs that is
            sorted ascendingly according to the priorities. This function is defined by the following
            conditional equations: 
            \begin{enumerate}
            \item $Q.\mathtt{isEmpty}() \rightarrow Q.\mathtt{toList}() = []$,
            \item $\neg Q.\mathtt{isEmpty}() \rightarrow Q.\mathtt{toList} = [Q.\mathtt{top}()] + Q.\mathtt{remove}().\mathtt{toList}()$.
            \end{enumerate}
      \item The function $\mathtt{insertList}$ takes a pair consisting of a priority and a value and inserts it
            into a sorted list of priority-values-pairs such that the resulting list is still sorted.
            This function has the signature
            \\[0.2cm]
            \hspace*{-0.8cm}
            $\mathtt{insertList}: \mathtt{Priority} \times \mathtt{Value} \times \mathtt{List}(\mathtt{Priority} \times \mathtt{Value}) 
            \rightarrow \mathtt{List}(\mathtt{Priority} \times \mathtt{Value})$.
            \\[0.2cm]
            This function can be specified as follows:
            \begin{enumerate}
            \item $\mathtt{insertList}(p,v,[]) = [\langle p,v\rangle ]$,
            \item $p_1 <    p_2 \rightarrow \mathtt{insertList}\bigl(p_1,v_1,[\pair(p_2,v_2)] + R\bigr) = [\pair(p_1,v_1), \pair(p_2,v_2)] + R$,
            \item $p_1 \geq p_2 \rightarrow \mathtt{insertList}\bigl(p_1,v_1,[\pair(p_2,v_2)] + R\bigr) = [\pair(p_2,v_2)] + \mathtt{insertList}\bigl(\pair(p_1,v_1), R\bigr)$.
            \end{enumerate}
      \end{enumerate}
      Now we can specify the behaviour of the abstract data type $\mathtt{PrioQueue}$.
      \begin{enumerate}
      \item $\mathtt{prioQueue}().\mathtt{isEmpty}() = \mathtt{true}$

            The constructor $\mathtt{prioQueue}$ creates an empty queue.
      \item $Q.\mathtt{insert}(p, v).\mathtt{isEmpty}() = \mathtt{false}$

            If something is inserted into a priority queue, the resulting queue is not empty.
      \item $\mathtt{prioQueue}().\mathtt{top}() = \Omega$

            If we try to retrieve the pair with the highest priority from an empty priority queue, the
            undefined value $\Omega$ is returned instead.
      \item $\neg Q.\mathtt{isEmpty}() \rightarrow Q.\mathtt{top}() = Q.\mathtt{toList}()[1]$

            If we retrieve the  pair with the highest priority from an non-empty priority queue $Q$,
            we get the same pair that is the first element of the list $Q.\mathtt{toList}()$.

      \item $Q.\mathtt{isEmpty}() \rightarrow Q.\mathtt{remove}() = Q$

            Trying to remove the top element from an empty queue $Q$ returns $Q$.
      \item $\neg Q.\mathtt{isEmpty}() \rightarrow Q.\mathtt{remove}().\mathtt{toList}() =
        Q.\mathtt{toList}()[2..]$

            If we remove the top element from a non-empty queue $Q$ and then transform the resulting queue into
            a sorted list, we get the same list that we get when we chop of the first element from the list
            $Q.\mathtt{toList}()$.
      \item $Q.\mathtt{insert}(p,v).\mathtt{toList}() = \mathtt{insertList}(p,v,Q.\mathtt{toList}())$

            If a pair $\pair(p,v)$ is inserted into a priority queue $Q$ and the resulting priority queue is
            converted into a list, then the resulting list is the same as if this pair is inserted
            into $Q.\mathtt{toList}()$.
      \end{enumerate}
\end{enumerate}
\end{Definition}
We could implement the ADT $\mathtt{PrioQueue}$ as a list of pairs that is sorted ascendingly.
Then, the different methods of $\mathtt{PrioQueue}$ would be implemented as follows:
\begin{enumerate}
\item $\mathtt{prioQueue}()$ returns an empty list.
\item $Q.\mathtt{insert}(p,v)$ is implemented by the function $\mathtt{insertList}$.  Note that we have
      implemented this function already when we had implemented the sorting algorithm ``\blue{insertion sort}''.
\item $Q.\mathtt{top}()$ return the first element from the list $Q$.
\item $Q.\mathtt{remove}()$ removes the first element from the list $Q$.
\end{enumerate}
The worst case complexity of this approach would be linear for the method $\mathtt{insert}()$,
i.e.~it would have complexity $\Oh(n)$ where $n$ is the number of elements in $Q$. 
All other operations would have the complexity $\Oh(1)$.  
Next, we introduce a more efficient implementation such that the complexity of $\mathtt{insert}()$ 
is only $\Oh\bigl(\log(n)\bigr)$.  To this end, we introduce
\href{https://en.wikipedia.org/wiki/Heap_(data_structure)}{Heaps} next. 

\section[Heaps]{The $\mathtt{Heap}$ Data Structure}
We define the set \href{https://en.wikipedia.org/wiki/Heap_(data_structure)}{Heap}\footnote{
In computer science, the notion of a \blue{Heap} is used for two different concepts:
First, a \blue{heap} is a data structure that is organized as a tree.  This kind of data structure
is described in more detail in this section. Second, the part of main memory that contains dynamically
allocated objects is known as \blue{the heap}.}
inductively as a subset of the set $\Bin$ of binary trees.  To this end, we first define a relation
\\[0.2cm]
\hspace*{1.3cm}
$\leq \;\subseteq \mathtt{Priority}  \times \mathcal{B}$
\\[0.2cm]
For a priority $p \in \mathtt{Priority}$ and a binary tree $b \in \Bin$ we have  $p \leq b$ 
if and only if $p \leq q$ for every priority $q$ occurring in $b$.  The formal definition of 
$p \leq b$ is as follows:
\begin{enumerate}
\item $p \leq \mathtt{Nil}$,

      because there are no priorities in the empty tree $\mathtt{Nil}$.
\item $p \leq \mathtt{Node}(q,v,l,r) \;\stackrel{\mbox{\scriptsize def}}{\longleftrightarrow}\; p \leq q \;\wedge\; p \leq l \;\wedge\; p \leq r$,
         
      because $p$ is less than or equal to every priority in the binary tree 
      $\mathtt{Node}(q,v,l,r)$ iff  $p \leq q$ and if, furthermore, 
      $p$ is less than or equal to every priority occurring in either  $l$ or $r$.
\end{enumerate}
Next, we define a function \\[0.1cm]
\hspace*{1.3cm} $\mathtt{count}: \Bin \rightarrow \mathbb{N}$, \\[0.1cm]
that counts the number of nodes occurring in a binary tree $b$.  The definition of
$b.\mathtt{count}()$ is given by induction on $b$.
\begin{enumerate}
\item $\mathtt{Nil}.\mathtt{count}() = 0$.
\item $\mathtt{Node}(p,v,l,r).\mathtt{count}() = 1 + l.\mathtt{count}() + r.\mathtt{count}()$.
\end{enumerate}
Now we are ready to define the set $\mathtt{Heap}$ by induction:
\begin{enumerate}
\item $\mathtt{Nil} \in \mathtt{Heap}$.
\item $\mathtt{Node}(p,v,l,r) \in \mathtt{Heap}$ if and only if the following is true:
      \begin{enumerate}
      \item $p \leq l \;\wedge\; p \leq r$

            The priority stored at the root is less than or equal to every other priority stored in
            the heap. This condition is known as the \blue{heap condition}.
      \item $\mid l.\mathtt{count}() - r.\mathtt{count}() \mid \;\leq\, 1$

            The number of elements in the left subtree differs from the number of elements stored in
            the right subtree by at most one.
            This condition is known as the  \blue{balancing condition}.  It is similar to the
            balancing condition of AVL trees, but instead of comparing the heights, this condition
            compares the number of elements.
      \item $l \in \mathtt{Heap} \;\wedge\; r \in \mathtt{Heap}$

            This condition ensures that all subtrees are heaps, too.
      \end{enumerate}
\end{enumerate}
The  \blue{heap condition} implies that in a non-empty heap the element with a highest priority is
stored at the root.  Figure \ref{fig:heap-list} on page \pageref{fig:heap-list} shows a simple heap.
In the upper part of the nodes we find the priorities.  Below these priorities we have the values
that are stored in the heap.  In the example given, the priorities are natural numbers, while the
values are characters.


\begin{figure}[!t]
  \centering
  \framebox{\epsfig{file=Abbildungen/heap-with-holes,scale=0.7}} 
  \caption{A heap.}
  \label{fig:heap-list}
\end{figure}

As heaps are binary trees, we can implement them in a fashion that is similar to our implementation
of AVL trees.  In order to do so, we first present equations that specify the methods of the data
structure heap.  We start with the method $\mathtt{top}$.  
\begin{enumerate}
\item $\mathtt{Nil}.\mathtt{top}() = \Omega$.
\item $\mathtt{Node}(p,v,l,r).\mathtt{top}() = \pair(p,v)$,

      because the heap condition ensures that the value with the highest priority is stored at the
      top. 
\end{enumerate}
Implementing the method $\mathtt{isEmpty}$ is straightforward:
\begin{enumerate}
\item $\mathtt{Nil}.\mathtt{isEmpty}() = \mathtt{true}$,
\item $\mathtt{Node}(p,v,l,r).\mathtt{isEmpty}() = \mathtt{false}$.
\end{enumerate}
When  implementing the method $\mathtt{insert}$ we have to make sure that both the balancing condition
and the heap condition are maintained.
\begin{enumerate}
\item $\mathtt{Nil}.\mathtt{insert}(p,v) = \mathtt{Node}(p,v,\mathtt{Nil}, \mathtt{Nil})$.
\item $p_{\mathrm{top}} \leq p \;\wedge\; l.\mathtt{count}() \leq r.\mathtt{count}() \;\rightarrow $   \\[0.1cm]
      \hspace*{1.3cm} 
      $\mathtt{Node}(p_{\mathrm{top}},v_\mathrm{top},l,r).\mathtt{insert}(p,v) =
                 \mathtt{Node}\bigl(p_\mathrm{top},v_\mathrm{top},l.\mathtt{insert}(p,v), r\bigr)$.
                 
      If the value $v$ to be inserted has a priority that is lower (or the same) than the priority of
      the value at the root of the heap, we have to insert the value $v$ either in the left or right
      subtree.  In order to maintain the balancing condition, we insert the value $v$ in the left
      subtree if that subtree stores at most as many values as the right subtree.
\item $p_{\mathrm{top}} \leq p \;\wedge\; l.\mathtt{count}() > r.\mathtt{count}() \;\rightarrow $   \\[0.1cm]
      \hspace*{1.3cm} 
      $\mathtt{Node}(p_{\mathrm{top}},v_\mathrm{top},l,r).\mathtt{insert}(p,v) =
                 \mathtt{Node}\bigl(p_\mathrm{top},v_\mathrm{top},l,r.\mathtt{insert}(p,v)\bigr)$.

      If the value $v$ to be inserted has a priority that is lower (or the same) than the priority of
      the value at the root of the heap, we have to insert the value $v$ in the right
      subtree if the right subtree  stores fewer values than the left subtree.
\item $p_{\mathrm{top}} > p \;\wedge\; l.\mathtt{count}() \leq r.\mathtt{count}() \;\rightarrow $ \\[0.1cm]
      \hspace*{1.3cm} 
      $\mathtt{Node}(p_{\mathrm{top}},v_\mathrm{top},l,r).\mathtt{insert}(p,v) =
                 \mathtt{Node}\bigl(p,v,l.\mathtt{insert}(p_\mathrm{top},v_\mathrm{top}), r\bigr)$.

      If the value $v$ to be inserted is associated with a priority $p$ that is higher than the priority of
      the value stored at the root of the heap, then we have to store the value $v$ at the root.
      The value $v_\mathrm{top}$ that was stored previously at the root has to be moved to either
      the left or right subtree.  If the number of nodes in the left subtree is as most as big as
      the number of nodes in the right subtree, $v_\mathrm{top}$ is inserted into the left subtree.
\item $p_{\mathrm{top}} > p \;\wedge\; l.\mathtt{count}() > r.\mathtt{count}() \;\rightarrow $ \\[0.1cm] 
      \hspace*{1.3cm} 
      $\mathtt{Node}(p_{\mathrm{top}},v_\mathrm{top},l,r).\mathtt{insert}(p,v) =
                 \mathtt{Node}\bigl(p,v,l,r.\mathtt{insert}(p_\mathrm{top},v_\mathrm{top})\bigr)$.

      If the value $v$ to be inserted is associated with a priority $p$ that is higher than the priority of
      the value stored at the root of the heap, then we have to store the value $v$ at the root.
      The value $v_\mathrm{top}$ that was stored previously at the root has to be moved to 
      the right subtree provided the number of nodes in the left subtree is bigger than
      the number of nodes in the right subtree.
\end{enumerate}
Finally, we specify our implementation of the method $\mathtt{remove}$.
\begin{enumerate}
\item $\mathtt{Nil}.\mathtt{remove}() = \mathtt{Nil}$,

      since we cannot remove anything from the empty heap.
\item $\mathtt{Node}(p,v,\mathtt{Nil},r).\mathtt{remove}() = r$,
  
\item $\mathtt{Node}(p,v,l,\mathtt{Nil}).\mathtt{remove}() = l$,

      because we always remove the value with the highest priority and this value is stored at the
      root.  Now if either of the two subtrees is empty, we can just return the other subtree.

      Next, we discuss those cases where none of the subtrees is empty.
      In that case, either the value that is stored at the root of the left subtree or the value
      stored at the root of the right subtree has to be promoted to the root of the tree.
      In order to maintain the heap condition, we have to choose the value that is associated with the
      higher priority.
\item $l = \mathtt{Node}(p_1,v_1,l_1,r_1) \;\wedge\; r = \mathtt{Node}(p_2,v_2,l_2,r_2) \;\wedge\; p_1 \leq p_2 \;\rightarrow$ \\[0.1cm] 
      \hspace*{1.3cm} 
      $\mathtt{Node}(p,v,l,r).\mathtt{remove}() =      \mathtt{Node}(p_1,v_1,l.\mathtt{remove}(),r)$,

      because if the value at the root of the left subtree has a higher priority than the value
      stored at the right subtree, then the value at the left subtree is moved to the root of the tree.
      Of course, after moving this value to the root, we have to recursively delete this value from
      the left subtree.
\item $l = \mathtt{Node}(p_1,v_1,l_1,r_1) \;\wedge\; r = \mathtt{Node}(p_2,v_2,l_2,r_2) \;\wedge\; p_1 > p_2 \rightarrow$ \\[0.1cm]
      \hspace*{1.3cm} 
      $\mathtt{Node}(p,v,l,r).\mathtt{remove}() = \mathtt{Node}(p_2,v_2,l,r.\mathtt{remove}())$

      This case is similar to the previous case, but now the value from the right subtree moves to
      the root.
\end{enumerate}
The hawk-eyed reader will have noticed that the specification of the method $\mathtt{delete}$ that is given
above violates the balancing condition.  It is not difficult to change the implementation so that
the balancing condition is maintained.  However, it is not really necessary to maintain the
balancing condition when deleting values.  The reason is that the balancing condition is needed as
long as the heap grows in order to guarantee logarithmic  performance.  However, when we remove
values from a priority queue, the height of the queue only shrinks.  Therefore, even if the heap
would degenerate into a list during removal of values, this would not be a problem because the
height of the tree would still be bounded by $\log_2(n)$, where $n$ is the maximal number of
values that a stored in the heap at any moment in time.

\exercise
Change the equations for the method $\mathtt{remove}$ so that the resulting heap satisfies the
balancing condition.



\section[Implementation]{Implementing \textsl{Heaps} in \textsc{SetlX}}

\begin{figure}[bt]
\centering
\begin{Verbatim}[ frame         = lines, 
                  framesep      = 0.3cm, 
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.0cm,
                  xrightmargin  = 0.0cm,
                ]
    class heap() {
        mPriority := om;
        mValue    := om;
        mLeft     := om;
        mRight    := om;
        mCount    := 0;
    
      static {
          top     := procedure()     { return [mPriority, mValue]; };
          insert  := procedure(p, v) { ... };
          remove  := procedure()     { ... };
          update  := procedure(t)    { ... };
          isEmpty := [] |-> mCount == 0;
      }
    }
\end{Verbatim}
\vspace*{-0.3cm}
\caption{Outline of the class $\mathtt{heap}$.}
\label{fig:heap.stlx-outline}
\end{figure}

\noindent
Next, we present an implementation of heaps in \textsc{SetlX}. 
Figure \ref{fig:heap.stlx-outline} shows an outline of the class $\mathtt{heap}$.  An object of class
heap represents a node in a heap data structure. In order to do this, it maintains the following
member variables:
\begin{enumerate}
\item $\mathtt{mPriority}$ is the priority of the value stored at this node,
\item $\mathtt{mValue}$    stores the corresponding value,
\item $\mathtt{mLeft}$ and $\mathtt{mRight}$ represent the left and right subtree, respectively, while
\item $\mathtt{mCount}$    gives the number of nodes in the subtree rooted at this node.
\end{enumerate}
The constructor initializes these member variables in a way that the resulting object represents an
empty heap.  Since a heap stores the value with the highest priority at the root, implementing the
method $\mathtt{top}$ is trivial: We just have to return the value stored at the root.
The implementation of $\mathtt{isEmpty}$ is easy, too: We just have to check whether the number of
values stored into this heap is zero.
\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines, 
                  framesep      = 0.3cm, 
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    insert := procedure(priority, value) {
        if (isEmpty()) {
            this.mPriority := priority;
            this.mValue    := value;
            this.mLeft     := heap(this);
            this.mRight    := heap(this);
            this.mCount    := 1;
            return;
        }
        this.mCount += 1;
        if (priority < mPriority) {                         
            if (mLeft.mCount > mRight.mCount) {
                mRight.insert(mPriority, mValue);
            } else {
                mLeft.insert(mPriority, mValue);
            }
            this.mPriority := priority;
            this.mValue    := value;
        } else {
            if (mLeft.mCount > mRight.mCount) { 
                mRight.insert(priority, value);
            } else {
                mLeft.insert(priority, value);
            }
        }
    };
\end{Verbatim}
\vspace*{-0.3cm}
\caption{Implementation of the method $\mathtt{insert}$.}
\label{fig:heap.stlx-insert}
\end{figure}

\noindent
Figure \ref{fig:heap.stlx-insert} show the implementation of the method $\mathtt{insert}$.
Basically, there are two cases.
\begin{enumerate}
\item If the given heap is empty, then we store the value to be inserted at the current node.
      We have to make sure to set $\mathtt{mLeft}$ and $\mathtt{mRight}$ to empty heaps.  The reason is
      that, for every non-empty node, we want $\mathtt{mLeft}$ and $\mathtt{mRight}$ to store objects.
      Then, we can be sure that an expression like $\mathtt{mLeft}.\mathtt{isEmpty}()$ is always well defined.
      If, however, we would allow $\mathtt{mLeft}$ to have the value $\mathtt{om}$, then the evaluation
      of $\mathtt{mLeft}.\mathtt{isEmpty}()$ would result in an error.
\item If the given heap is non-empty, we need another case distinction.
  \begin{enumerate}
  \item If the $\mathtt{priority}$ of the $\mathtt{value}$ to be inserted is higher than
        $\mathtt{mPriority}$, which is the priority of the value at the current node, then we have to 
        put $\mathtt{value}$ at the current node, overwriting $\mathtt{mValue}$.  However, as we do not want
        to lose the value $\mathtt{mValue}$ that is currently stored at this node, we have to insert 
        $\mathtt{mValue}$ into either the left or the right subtree.  In order to keep the heap
        balanced we insert $\mathtt{mValue}$ into the smaller subtree and choose the left subtree if
        both subtrees have the same size.
  \item If the $\mathtt{value}$ to be inserted has a lower $\mathtt{priority}$ than $\mathtt{mPriority}$, then we have
        to insert $\mathtt{value}$ into one of the subtrees.  Again, in order to maintain the balancing
        condition, $\mathtt{value}$ is stored into the smaller subtree.
  \end{enumerate}
\end{enumerate}

\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines, 
                  framesep      = 0.3cm, 
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    remove := procedure() {
        this.mCount -= 1;
        if (mLeft.isEmpty()) { 
            update(mRight); 
            return;
        } 
        if (mRight.isEmpty()) { 
            update(mLeft ); 
            return;
        }
        if (mLeft.mPriority < mRight.mPriority) {
            this.mPriority := mLeft.mPriority;
            this.mValue    := mLeft.mValue;
            mLeft.remove();
        } else {
            this.mPriority := mRight.mPriority;
            this.mValue    := mRight.mValue;
            mRight.remove();
        }
    };
\end{Verbatim}
\vspace*{-0.3cm}
\caption{Implementation of the method $\mathtt{remove}$.}
\label{fig:heap.stlx-remove}
\end{figure}

\noindent
Figure \ref{fig:heap.stlx-remove} shows the implementation of the method $\mathtt{remove}$.  This
method removes the value with the highest priority from the heap.  Essentially, there are two
cases.
\begin{enumerate}
\item If the left subtree is empty, we replace the given heap with the right subtree. 
      Conversely, if the right subtree is empty, we replace the given heap with the  left subtree.
\item Otherwise, we have to check which of the two subtrees contains the value with the highest
      priority.  This value is then stored at the root of the given tree and, of course,
      it has to be removed from the subtree that had stored it previously.
\end{enumerate}

\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines, 
                  framesep      = 0.3cm, 
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    update := procedure(t) {
        this.mPriority := t.mPriority;
        this.mValue    := t.mValue;
        this.mLeft     := t.mLeft;
        this.mRight    := t.mRight;
        this.mCount    := t.mCount;
    };      
\end{Verbatim}
\vspace*{-0.3cm}
\caption{Implementation of the method $\mathtt{update}$.}
\label{fig:heap.stlx-update}
\end{figure}

\noindent 
Figure \ref{fig:heap.stlx-update} shows the implementation of the auxiliary method $\mathtt{update}$.
Its implementation is straightforward: It copies the member variables stored at the node $\mathtt{t}$
to the node $\mathtt{this}$.  This method is needed since in \textsc{SetlX}, assignments of the form
\\[0.2cm]
\hspace*{1.3cm}
\texttt{this := mLeft;} \quad or \quad \texttt{this := mRight;}
\\[0.2cm]
are not permitted.


\exercise
The implementation of the method $\mathtt{remove}$ given above violates the balancing condition.
Modify the implementation of $\mathtt{remove}$ so that the balancing condition remains valid.

\exercise
Instead of defining a class with member variables $\mathtt{mLeft}$ and $\mathtt{mRight}$, a binary tree
can be stored as a list $l$.  In that case, for every index $i \in \{1, \cdots, \mathtt{\#}l \}$,
the expression $l[i]$ stores a node of the tree.  The crucial idea is that the left subtree of the
subtree stored at the index $i$ is stored at the index $2 \cdot i$, while the right subtree is
stored at the index $2 \cdot i + 1$.  Develop an implementation of heaps that is based on this idea.

\section{Heap-Sort}
Heaps can be used to implement a sorting algorithm that is efficient in terms of both time and
memory. While merge sort needs only $n \cdot \log_2(n)$ comparisons to get the job done, the
algorithm uses an auxiliary array and is therefore not optimally efficient with regard to its memory
consumption.  The algorithm we describe next, \href{https://en.wikipedia.org/wiki/Heapsort}{heapsort} has
a time complexity that is $\Oh\bigl(n \cdot \log_2(n)\bigr)$ and does not require an auxiliary
array.  Heapsort was invented by J.W.J.~Williams and
\href{https://en.wikipedia.org/wiki/Robert_W._Floyd}{Robert W.~Floyd} in 1964.

Given an array $\mathtt{A}$ of keys to be sorted, the basic version of Heapsort proceeds as follows:
\begin{enumerate}
\item The elements of $\mathtt{A}$ are inserted in a heap $\mathtt{H}$.
\item Now the smallest element of $\mathtt{A}$ is at the top of $\mathtt{H}$.  Therefore, if we remove the elements
      from $\mathtt{H}$ one by one, we retrieve these elements in increasing order.
\end{enumerate}
A basic implementation of heapsort that is based on this idea is given in Figure
\ref{fig:basic-heapsort.stlx} on page \pageref{fig:basic-heapsort.stlx}.  This implementation makes
use of the class $\mathtt{heap}$ that had been presented in the previous section.
\begin{enumerate}
\item In order to sort the list $\mathtt{A}$ that is given as argument to $\mathtt{heapSort}$, we first
      create the empty heap 
      $\mathtt{H}$ in line 2 and then proceed to insert all elements of the list $\mathtt{A}$ into $\mathtt{H}$ 
      in line 4.
\item Next we create an empty list $\mathtt{S}$ in line 5. When the procedure $\mathtt{heapSort}$
      finishes, this list will be a sorted version of the list $\mathtt{A}$.
\item As long as the heap $\mathtt{H}$ is not empty, we take its top element and append it to
      $\mathtt{S}$.  Since the method $\mathtt{top}$ returns a pair of the form $\langle \mathtt{p}, \mathtt{v}\rangle$ and
      we are only interested in the priority $\mathtt{p}$, we just add the first element of this pair to the
      end of the list $\mathtt{S}$.  After we have appended $\mathtt{p}$ to the list $\mathtt{S}$, the pair
      is removed $\langle \mathtt{p}, \mathtt{v}\rangle$ from the heap $\mathtt{H}$.
\item Once the heap $\mathtt{H}$ has become empty, $\mathtt{S}$ contains all of the elements of the list $\mathtt{A}$
      sorted ascendingly.
\end{enumerate}

\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines, 
                  framesep      = 0.3cm, 
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    heapSort := procedure(A) {
        H := heap();    
        for (x in A) {
            H.insert(x, x);
        }
        print(H);
        S := [];
        while (!H.isEmpty()) {
            S += [ H.top()[1] ];
            H.remove();
        }
        return S;
    };
\end{Verbatim}
\vspace*{-0.3cm}
\caption{A basic version of heapsort.}
\label{fig:basic-heapsort.stlx}
\end{figure}

The basic version of heapsort that is shown in Figure \ref{fig:basic-heapsort.stlx} can be improved
by noting that a heap can be stored efficiently in an array $\mathtt{A}$.  If a node of the form
$\mathtt{Node}(p, v, l, r)$ is stored at index $i$, then the left subtree $l$ is stored at
index $2 \cdot i$ while the right subtree $r$ is stored at index $2 \cdot i + 1$:
\\[0.2cm]
\hspace*{1.3cm}
$\mathtt{A}[i] \doteq \mathtt{Node}(p, v, l, r) \;\rightarrow\; \mathtt{A}[2\cdot i] \doteq l \;\wedge\; \mathtt{A}[2\cdot i+1] \doteq r$.
\\[0.2cm]
Here, the expression $\mathtt{A}[i] \doteq \mathtt{Node}(p, v, l, r)$ is to be read as 
\\[0.2cm]
\hspace*{1.3cm}
``The root of the heap $\mathtt{Node}(p, v, l, r)$ is stored at index $i$ in the array $\mathtt{A}$''.
\\[0.2cm]
If we store a heap in this manner, then instead of using pointers that point to the left and right
subtree of a node we can just use index arithmetic to retrieve the subtrees.  


 
\begin{figure}[!ht]
\centering
\begin{Verbatim}[ frame         = lines, 
                  framesep      = 0.3cm, 
                  firstnumber   = 1,
                  labelposition = bottomline,
                  numbers       = left,
                  numbersep     = -0.2cm,
                  xleftmargin   = 0.8cm,
                  xrightmargin  = 0.8cm,
                ]
    swap := procedure(x, y, rw A) {
        [A[x], A[y]] := [A[y], A[x]];
    };
    sink := procedure(k, rw A, n) {
        while (2 * k <= n) {
            j := 2 * k;
            if (j < n && A[j] > A[j+1]) {
                j += 1;
            }
            if (A[k] < A[j]) {
                return;
            }
            swap(k, j, A);
            k := j;
        }
    };
    heapSort := procedure(rw A) {
        n := #A;
        for (k in [n\2, n\2-1 .. 1]) {
            sink(k, A, n);
        }
        while (n > 1) {
            swap(1, n, A);
            n -= 1;
            sink(1, A, n);
        }
    };
\end{Verbatim}
\vspace*{-0.3cm}
\caption{An implementation of Heapsort in \textsc{SetlX}.}
\label{fig:heap-sort.stlx}
\end{figure}

Figure \ref{fig:heap-sort.stlx} on page \pageref{fig:basic-heapsort.stlx} makes use of this idea.
We discuss this implementation line by line.
\begin{enumerate}
\item The function $\mathtt{swap}$ exchanges the elements in the array $\mathtt{A}$ that are at the
      positions $\mathtt{x}$ and $\mathtt{y}$.
\item The procedure $\mathtt{sink}$ takes three arguments.
      \begin{enumerate}
      \item $\mathtt{k}$ is an index into the array $\mathtt{A}$.
      \item $\mathtt{A}$ is the array representing the heap.
      \item $\mathtt{n}$ is the size of the part of this array that has to be sorted.  

            The array $\mathtt{A}$ itself might actually have more than $\mathtt{n}$ elements, but for the
            purpose of the method $\mathtt{sink}$ we restrict our attention to the subarray
            $\mathtt{A[1..n]}$. 
      \end{enumerate}
      When calling $\mathtt{sink}$, the assumption is that $\mathtt{A}[\mathtt{k..n}]$ should represent a heap 
      that possibly has its heap condition violated at its root, i.e.~at index $\mathtt{k}$.  The
      purpose of the procedure $\mathtt{sink}$ is to restore the heap condition at index $\mathtt{k}$.
      To this end, we first compute the index $\mathtt{j}$ of the left subtree below index $\mathtt{k}$.
      Then we check whether there also is a right subtree at position $\mathtt{j}+1$, which is the
      case if $\mathtt{j}$ is less than $\mathtt{n}$.  Now if the heap condition is violated at index
      $\mathtt{k}$, we have to exchange the element at  position $\mathtt{k}$ with the child that has
      the higher priority, i.e.~the child that is smaller. Therefore, in line 8 we arrange for index
      $\mathtt{j}$ to point to the smaller child.  Next, we check in line 10 whether the heap
      condition is violated at index $\mathtt{k}$.  If the heap condition is satisfied, there is
      nothing left to do and the procedure returns.  Otherwise, the element at position $\mathtt{k}$ is swapped with
      the element at position $\mathtt{j}$.  Of course, after this swap it is possible that the heap condition is
      violated at position $\mathtt{j}$.  Therefore,  $\mathtt{k}$ is set to $\mathtt{j}$ and the \texttt{while}-loop continues
      as long as the node at position $\mathtt{k}$ has a child, i.e.~as long as 
      $2 \cdot \mathtt{k}\leq \mathtt{n}$.
\item The procedure $\mathtt{heapSort}$ has the task to sort the array $\mathtt{A}$ and proceeds in two phases.
      \begin{enumerate}
      \item In phase 1 our goal is to transform the array $\mathtt{A}$ into a heap that is stored in $\mathtt{A}$.

            In order to do so, we traverse the array $\mathtt{A}$ in reverse using the
            \texttt{for}-loop starting in line 19.  The invariant of this loop is that before
            $\mathtt{sink}$ is called, all trees rooted at an index greater than
            $\mathtt{k}$ satisfy the heap condition.  Initially this is true because the trees that
            are rooted at indices greater than $\mathtt{n}\backslash 2$ are trivial, i.e.~they only
            consist of their root node.  Then, since there are no children below these nodes, the heap
            condition is satisfied vacuously. 
            
            In order to satisfy the invariant for index $\mathtt{k}$, $\mathtt{sink}$ is called with
            argument $\mathtt{k}$,  since at this point, the tree rooted at index $\mathtt{k}$ satisfies
            the heap condition except possibly at the root.  It is then the job of $\mathtt{sink}$ to
            establish the heap condition for index $\mathtt{k}$.  If the element at the root has a
            priority that is too low, $\mathtt{sink}$ ensures that this element sinks down in the tree
            as far as necessary.
      \item In phase 2 we remove the elements from the heap one-by-one and insert them at the end of
            the array.

            When the \texttt{while}-loop starts, the array $\mathtt{A}$ contains a heap.  Therefore,
            the smallest element is found at the root of the heap.  Since we want to sort the
            array $\mathtt{A}$ descendingly, we move this element to the end of the array $\mathtt{A}$ and in
            return move the element from the end of the array $\mathtt{A}$ to the front.
            After this exchange, the sublist \texttt{a[1..n-1]} represents a heap, except that the
            heap condition might now be violated at the root.  Next, we decrement $\mathtt{n}$ in line 24, since the
            last element of the array $\mathtt{A}$ is already in its correct position.  
            In order to reestablish the heap condition at the root, we call $\mathtt{sink}$ with index
            \texttt{1} in line 25.

            The \texttt{while}-loop runs as long as the part of the array that has to be sorted has
            a length greater than 1.  If there is only one element left in this part of the array, this part is
            trivially sorted and the \texttt{while}-loop terminates.
      \end{enumerate}
\end{enumerate}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "algorithms"
%%% End: 
